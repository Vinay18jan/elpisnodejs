import SqlHelper from "../Utility/MySqlhelper";
import { isNullOrUndefined } from "util";
import { EmployeeGetType } from "../Types/EmployeeGetType";

export let GetEmployeeDetailById = (req, res, done: Function) => {
    let sqlHelper = new SqlHelper();
    var params = {
        'p_id': req.params.id
    };
    sqlHelper.ExecuteSP('get_employee_details_by_id', params, function (error, results) {

        let searchResult: EmployeeGetType = new EmployeeGetType();
        if (error) {
            done(error, undefined);
        }
        else if (!isNullOrUndefined(results) && !isNullOrUndefined(results[0]) && results[0].length > 0) {
            if (results[0] != undefined) {
                searchResult.Id = results[0][0].id;
                searchResult.Name = results[0][0].name;
            }

            done(undefined, searchResult);
        }
        else done(undefined, undefined);
    });
}

export const AddEmployee = (req, res, done: Function) => {
    try {
        let sqlHelper = new SqlHelper();
        var params = {
            'p_id': req.body.employeeid,
            'p_employee_name': req.body.empname
        }
        sqlHelper.ExecuteSP('add_employee', params, function (error, result) {
            if (error) {
                done(error, undefined);
            }
            else if (result.length > 0) {
                done(undefined, result[0][0]);
            }
        })
    }
    catch (ex) {
        done(ex, undefined)
    }
}
