import express from 'express';
import * as controller from '../Controller/CommonController';
let router = express.Router();

router.get('/datetime',controller.GetDatetime);
router.get('/employee/:id',controller.GetEmployeeDetailById);
router.post('/employee',controller.AddEmployee);

export default router;