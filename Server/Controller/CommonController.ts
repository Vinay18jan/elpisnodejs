import { Request, Response, NextFunction } from "express";
import * as CommonModel from '../Model/CommonModel';

export let GetDatetime = (req: Request, res: Response, next: NextFunction) => {
    try {
        res.status(200).json({
            "status": true,
            "data": new Date(),
            "message": "Current Timestamp!!!!"
        });
        next();
    }
    catch(ex){
        next(ex);
    }
}

export let GetEmployeeDetailById = (req: Request, res: Response, next: NextFunction) => {
    try {
        CommonModel.GetEmployeeDetailById(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": result,
                    "message": ""
                });
                next();
            }
            else {
                res.status(404).json({
                    "status": false,
                    "data": "",
                    "message": 'NOT FOUND!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

export let AddEmployee = (req: Request, res: Response, next: NextFunction) => {
    try {
        CommonModel.AddEmployee(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": '',
                    "message": "Created Successfully!!!!"
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to create!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

